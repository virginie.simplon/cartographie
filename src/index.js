let url = 'https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=associations_chambery&rows=1000&facet=nom&facet=objet&facet=web&facet=quartier&facet=service&facet=rubrique';

function loadData(adress, callback) {
  document.getElementById('loading').classList.remove('hide');
  document.getElementById('checkboxesWrapper').classList.add('hide');

  fetch(adress)
    .then(response => {
      return response.json()
    })
    .then(data => {
      // Work with JSON data here
      callback(data)
      document.getElementById('loading').classList.add('hide');
      document.getElementById('checkboxesWrapper').classList.remove('hide');

    })
    .catch(e => {
      console.log(e);
    });
}

//! Leaflet
var map = L.map('mapid').setView([45.56594172865833, 5.921727418899536], 13);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox.streets',
  accessToken: 'pk.eyJ1IjoibWxmY250IiwiYSI6ImNqdTlydGc2ZzFlc2o0ZXFyYW51MjVmZG4ifQ.fPJYRDcuJ8XthMTbx5kcYA'
}).addTo(map);


let jsonLength = []
var layerGroup = L.layerGroup().addTo(map);

function addMarkers(data, placeholder) {
  // reset();
  for (var i = 0; i < data.records.length; i++) {
    if (data.records[i].fields.localisation === undefined) {
      continue;
    }
    if (data.records[i].fields.rubrique === placeholder) {

      jsonLength.push(data.records[i].fields);
      var name = (data.records[i].fields.nom);
      if (name === undefined) {
        name = "Non renseigné";
      };

      var long = (data.records[i].fields.localisation[1]);
      var lat = (data.records[i].fields.localisation[0]);
      var adr1 = (data.records[i].fields.adr1);
      if (adr1 === undefined) {
        adr1 = " ";
      };
      var adr2 = (data.records[i].fields.adr2);
      if (adr2 === undefined) {
        adr2 = " ";
      };
      var adr3 = (data.records[i].fields.adr3);
      if (adr3 === undefined) {
        adr3 = " ";
      };
      var objet = (data.records[i].fields.objet);
      if (objet === undefined) {
        objet = "Non renseigné";
      };
      var rubrique = (data.records[i].fields.rubrique);
      if (rubrique === undefined) {
        rubrique = "Aucune";
      };
      var sousrubrique = (data.records[i].fields.sousrubrique);
      if (sousrubrique === undefined) {
        sousrubrique = "Aucune";
      };
      var nompres = (data.records[i].fields.nompres);
      if (nompres === undefined) {
        nompres = "Non renseigné";
      };
      var telpres = (data.records[i].fields.telpres);
      if (telpres === undefined) {
        telpres = "Non renseigné";
      };
      var mail = (data.records[i].fields.mail);
      if (mail === undefined) {
        mail = "Non renseigné";
      };
      var web = (data.records[i].fields.web);
      if (web === undefined) {
        web = "Non renseigné";
      };
      var marker = L.marker([lat, long]).addTo(layerGroup);
      marker.bindPopup(`<table>
      <caption>${name}</caption>
      <tr>
      <td>Objet : ${objet}</td>
     </tr>
      <tr>
          <td>Adresse : ${adr1} ${adr2} ${adr3}</td>
      </tr>
      
      <tr>
          <td>Rubrique : ${rubrique}</td>
      </tr>
      <tr>
          <td>Sous-rubrique : ${sousrubrique}</td>
      </tr>
     
      <tr>
          <td>Nom du président(e) : ${nompres}</td>
      </tr>
      <tr>
          <td>Téléphone du président(e) : ${telpres}</td>
      </tr>
      <tr>
          <td>Mail : ${mail}</td>
      </tr>
      <tr>
          <td>Web : ${web}</td>
      </tr>
  </table>`);

      generateList(name, adr1, adr2, adr3, rubrique, sousrubrique, objet, nompres, telpres, mail, web)

    }

  }
};


//? This function capitalize the first letter of every word in a string of text
function titleCase(str) {
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(' ');
}

// var arrayLat = [];

//TODO function spreadMarkers() {
//   for (i = 0; i < data.records.length; i++) {
//     if (data.records[i].fields.localisation === undefined) {
//       continue;
//     }
//     if (arrayLat.includes(data.records[i].fields.localisation[0] === false)) {
//       arrayLat.push(data.records[i].fields.localisation[0]);
//     } else {
//       arrayLat.push(data.records[i].fields.localisation[0] - 0.00005);
//     }
//   }
// }

// var arrayLong = [];

//TODO function spreadMarkers2() {
//   for (i = 0; i < data.records.length; i++) {
//     if (data.records[i].fields.localisation === undefined) {
//       continue;
//     }
//     if (arrayLong.includes(data.records[i].fields.localisation[1] === false)) {
//       arrayLong.push(data.records[i].fields.localisation[1]);
//     } else {
//       arrayLong.push(data.records[i].fields.localisation[1] + 0.00005);
//     }
//   }
// }


//TODO function showPages(id) {


//   var totalNumberOfPages = 5;
//   for (var i = 1; i <= totalNumberOfPages; i++) {

//     if (document.getElementById('page' + i)) {

//       document.getElementById('page' + i).style.display = 'none';
//     }

//   }
//   if (document.getElementById('page' + id)) {

//     document.getElementById('page' + id).style.display = 'block';
//   }
// };
//? This function generates a list which contains all the user requested results
function generateList(name, adr1, adr2, adr3, rubrique, sousrubrique, objet, nompres, telpres, mail, web) {
  var list = document.getElementById('resultsList');
  list.classList.remove('hide');
  list.innerHTML += ('<li class="collection-item"> <span class="accent-color"> Nom :</span> ' + titleCase(name) + '<br><span class="accent-color">Adresse :</span> ' + titleCase(adr1) + ' ' + titleCase(adr2) + ' ' + titleCase(adr3) + '<br></span> <span class="accent-color">Rubrique : </span> ' + titleCase(rubrique) + '<br> <span class="accent-color">Sous-rubrique :</span> ' + titleCase(sousrubrique) + '<br> <span class="accent-color">Objet :</span> ' + objet + '<br><span class="accent-color"> Président(e) : </span> ' + nompres + '<br> <span class="accent-color"> Téléphone  : </span> ' + telpres + '<br> <span class="accent-color"> Mail : </span> ' + mail + '<br> <span class="accent-color"> Site Web : </span> ' + web + '</li>');
}
//? This function generate a "Toast" message when the results button is pressed
function coolToast() {
  let text = `Carte générée. Total : ${jsonLength.length} résultats !`;

  M.toast({
    html: text,
    classes: 'toast',
  });
}


//? This function takes all the associations categories found in the json and put them in an array if they're not already in there. 
let arrayRubriques = [];

function recenserRubriques(data) {
  for (i = 0; i < data.records.length; i++) {
    if (arrayRubriques.includes(data.records[i].fields.rubrique) === false) arrayRubriques.push(data.records[i].fields.rubrique);
  }
}

//? This function displays the catogories from arrayRubriques with materializeCSS styling (checkboxes)
function createCheckboxes() {

  for (i = 0; i < arrayRubriques.length; i++) {
    document.getElementById('checkboxesWrapper').innerHTML += '<p class=\"col s6 m4 l3\">\r\n                    <label>\r\n                        <input type=\"checkbox\" \/>\r\n                        <span class="rubriquesClass"> <\/span>\r\n                    <\/label>\r\n                <\/p>';
    document.getElementsByClassName('rubriquesClass')[i].innerHTML += titleCase(arrayRubriques[i]);
  }
}

//? This function looks for checkbox that are checked and put the results in an array
let isChecked = [];

function isItChecked() {
  for (i = 0; i < arrayRubriques.length; i++) {
    if (document.getElementsByTagName('input')[i].checked === true) {
      document.getElementsByTagName('input')[i] = arrayRubriques[i];
      if (isChecked.includes(arrayRubriques[i]) === false) isChecked.push(arrayRubriques[i]);

    }
  }
};
//? Display the checked catagories in the collection header
function spanFilterContent() {
  let spanFilters = document.getElementById('spanFiltersid');
  spanFilters.innerHTML=(isChecked);
}

//? This function reset the markers and the collection of results
function reset() {
  var list = document.getElementById('resultsList');
  list.innerHTML = ('<li class=\"collection-header center-align collectiontitle\">\r\n                    <h4>Liste des r\u00E9sultats correspondants \u00E0 vos crit\u00E8res :<h4> <span\r\n                                class=\"accent-color-no-underline\" id=\"spanFiltersid\"><\/span>\r\n                <\/li>');
}
//? This function initializes the app
function initApp(data) {

  recenserRubriques(data);
  createCheckboxes();

}


//?TODO This function displays the results (list and markers on map)
function displayResults(data) {
  // spreadMarkers();
  // spreadMarkers2();
  reset();
  let mapid = document.getElementById('mapid');
  mapid.classList.remove('hide');
  map.invalidateSize()
  isChecked.length = 0;
  layerGroup.clearLayers();
  isItChecked();
  for (let i = 0; i < isChecked.length; i++) {
    addMarkers(data, isChecked[i]);
    spanFilterContent();
  }
  coolToast();


};

//? Events
document.getElementById("generateResults").addEventListener("click", () => {
  loadData(url, displayResults)
});
window.addEventListener("DOMContentLoaded", () => {
  loadData(url, initApp)
});
